import itertools
import time

def guessWord(word, string):
    start = time.time()
    chars = string
    tries = 0
    for i in range(1, 9):
        for letter in itertools.product(chars, repeat=i):
            tries += 1
            letter = ''.join(letter)
            if letter == word:
                end = time.time()
                guessTime = end - start
                return (tries, guessTime)


# word = input("Word : ")
word = "abcde"
chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
tries, guessTime = guessWord(word, chars)
print("Word %s guessed in %s tries and %s seconds!" % (word, tries, guessTime))