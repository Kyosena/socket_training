import socket
import sys
from datetime import datetime
import os


if len(sys.argv) != 3:
    print(len(sys.argv))
    print ('Usage: python3 client.py host port')
else :
    host = sys.argv[1]
    port = int(sys.argv[2])
    flag = 1
    while flag == 1:
        task = input("Choose task to execute: \n 1. Monitor availability \n 2. Execute a task \n")
        if(int(task) == 2):
            file = input("What file would you like to give? \n")
            with open(file,'r') as f:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                    file_size = os.path.getsize(file)
                    print("The file size = " + str(file_size))
                    size=int(file_size)
                    s.connect((host, port))
                    fs = s.makefile('rw')
                    fs.write("TASK")
                    fs.flush()
                    print("Waiting for job confirmation")
                    s.recv(64)
                    fs.write(str(file_size))
                    fs.flush()
                    # Waiting for server to confirm that they have received the header
                    print("Waiting for header confirmation.")
                    s.recv(64)

                    chunk = f.read(64)
                    count = 0
                    while chunk:
                        print("Sent data :" + str(len(chunk)) + " bytes.")
                        fs.write(chunk)
                        
                        size -= len(chunk)
                        # print("chunksize " + str(size))
                        if(size <= 0):
                            # print("masuk first flush")
                            fs.flush()
                            break

                        # print("sent 64 bytes, size left " + str(size))
                        count += 1
                        chunk = f.read(64)
                        if(len(chunk) == 0):
                            fs.flush()
                            break
                        # print("priming: " + chunk)
                    test = s.recv(64)
                    print("Sending finish chunk")
                    val = input("Finished sending data. Execute file?(Y/N) \n")
                    fs.write(val)
                    fs.flush()
                    confirmation = s.recv(64)
                    print(str(confirmation,"utf-8"))
                    with open("output.txt","w+") as o:
                        # print("flag1----")
                        outt = s.recv(256)
                        # print("flag2----")
                        while len(outt) >= 256:
                            o.write(str(outt,"utf-8"))
                            outt = s.recv(256)
                            print("Receiving " + str(len(outt)))
                            # print("flag3-----")
                        o.write(str(outt,"utf-8"))
                    printing = input("Job done. Would you like to print the output?(Y/N) \n")
                    if(printing.upper() == "Y"):
                        with(open("output.txt","r")) as r:
                            for line in r.readlines():
                                print(str(line))
        elif(int(task) == 1):
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.connect((host, port))
                fs=s.makefile("rw")
                fs.write("MONITOR")
                fs.flush()
                nodes = s.recv(256)
                with open("availability.txt","w") as a:
                    while nodes:
                        a.write(str(nodes,"utf-8"))
                        nodes = s.recv(256)
                        if(len(nodes) <= 256):
                            a.write(str(nodes,"utf-8    "))
                            break
                with open("availability.txt","r") as b:
                    for line in b.readlines():
                        print(line)
        cont = input("Done. Exit? (Yes/No) \n")
        if(cont.upper() == "NO"):
            continue
        elif(cont.upper() == "YES"):
            print("Thank you.")
            s.close()
            flag = 0
        else:
            print("Invalid choice.")
            s.close()
            flag = 0
                    


