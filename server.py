import socket
import sys
import os
import threading
buffersize = 256
if len(sys.argv) < 5 or len(sys.argv) % 2 != 1:
    print(len(sys.argv))
    print('Usage: python3 client.py server_host server_port node1_host node1_port node2_host node2_port...')
else:
    host = sys.argv[1]
    port = int(sys.argv[2])
    nodes = []
    for i in range(3, len(sys.argv), 2):
        nodes.append((sys.argv[i], int(sys.argv[i+1])))

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, port))
        s.listen()
        i = 0
        ping = 0
        while True:
            c,address = s.accept()
            with c:
                # print("connected to client at " + str(address))
                fc = c.makefile("rw")
                job_type=str(c.recv(buffersize),"utf-8")
                print("Received job type :" + job_type)
                if(job_type == "TASK"):
                    #sending job type confirmation
                    fc.write("job type confirmed.")
                    fc.flush()
                    header=str(c.recv(buffersize),"utf-8")
                    print("File size: " + header )
                    size=int(header)
                    fc.write("received header. Proceeding to execute.")
                    fc.flush()
                    while True:
                        if(ping < len(nodes)):
                            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as n:
                                node_host = nodes[i][0]
                                node_port = nodes[i][1]

                                i += 1
                                if i == len(nodes):
                                    i = 0

                                try:
                                    n.connect((node_host, node_port))
                                    fn=n.makefile("rw")
                                    fn.write("TASK")
                                    fn.flush()
                                    n.recv(64)
                                    fn.write(str(size))
                                    fn.flush()
                                    chunk = c.recv(buffersize)
                                    counter = 0
                                    while chunk:
                                        msg = str(chunk,"utf=8")
                                        print("Received data. from " + str(address) + ". data =" + str(len(chunk)) + " bytes.")
                                        counter += 1
                                        n.send(chunk)
                                        
                                        size -= len(chunk)
                                        # print(str(size))
                                        if(size <= 0):
                                            fn.flush()
                                            break
                                        chunk = c.recv(buffersize)
                                        fc.flush()
                                        # print(str(len(chunk)))
                                    c.send(bytes("ok","utf-8"))
                                    fc.flush()
                                    chunk2 = c.recv(buffersize)
                                    if(str(chunk2,"utf-8").upper() == "Y"):
                                        n.recv(64)
                                        n.send(bytes("execute","utf-8"))
                                        fn.flush()
                                        confirmation = "Job executed at %s %d." %(node_host,node_port)
                                        # print(str(confirmation,"utf-8"))
                                        c.send(bytes(confirmation,"utf-8"))
                                        fc.flush()
                                    print("Execution on node (%s,%d) successful. Waiting on data to receive." %(node_host, node_port))
                                    chunk3 = n.recv(buffersize)
                                    counter = len(chunk3)
                                    print("Output data received.")
                                    while chunk3:
                                        c.send(chunk3)
                                        chunk3 = n.recv(buffersize)
                                        counter += len(chunk3)
                                        print("sending " + str(len(chunk3)))
                                    fc.flush()
                                    print("Output data sent.")
                                    print("Output data size : " + str(counter))
                                    # print("execution message sent.")
                                    ping = 0

                                except ConnectionRefusedError:
                                    ping += 1
                                    print("Execution on node (%s,%d) failed, rerouting to node (%s,%d). Current ping = %d. Fatal ping = %d."
                                        %(node_host, node_port, nodes[i][0], nodes[i][1], ping,len(nodes)))
                                    continue

                            break
                        else:
                            print("No available nodes")
                            c.close()
                            break
                elif(job_type == "MONITOR"):
                    node_dict= {}
                    counter = len(nodes)
                    x = 0
                    while x < len(nodes):
                        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as n:
                            node_host = nodes[x][0]
                            node_port = nodes[x][1]
                            key = "IP address " + str(node_host) + ", Port " + str(node_port)
                            node_dict[key] = "Available."
                            try:
                                n.connect((node_host, node_port))
                                fn = n.makefile("rw")
                                fn.write("PING")
                                fn.flush()
                                print("Connection to %s.%d successful." %(node_host,node_port))
                                print("Loop ke " + str(x))
                                x += 1
                                n.close()
                                continue
                            except ConnectionRefusedError:
                                print("Connection to %s,%d unsuccessful. " %(node_host,node_port))
                                node_dict[key] = "Unavailable."
                                counter -= 1
                                print("Loop ke " + str(x))
                                x += 1  
                                continue
                    fc.write("%d out of %d nodes available. Complete statistics: "  %(counter,len(nodes)))
                    fc.flush()
                    for key in node_dict:
                        print(str(key))
                        print(str(node_dict[key]))
                        write = " " + str(key) + " " + str(node_dict[key])
                        fc.write(write)
                    fc.flush()

