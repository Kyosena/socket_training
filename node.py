import socket
import sys
import os
import subprocess

if len(sys.argv) != 2:
    print(len(sys.argv))
    print ('Usage: python3 node.py port')
else :
    host = ''
    port = int(sys.argv[1])

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, port))
        s.listen()
        fs = s.makefile("rw")

        while True:
            with s.accept()[0] as conn:
                task = conn.recv(64)
                if(str(task,"utf-8") == "TASK"):
                    conn.send(bytes("confirmation.", "utf-8"))
                    header= str(conn.recv(64),"utf-8")
                    size = int(header)
                    print("Header received. File to accept: " + header + " bytes.")
                    with open("job.py", "w") as f:
                        chunk = conn.recv(1024)
                        # print("received bytes: " + str(chunk,"utf-8"))
                        while chunk:
                            print("Received " + str(len(chunk)) + " bytes.")
                            f.write(str(chunk, 'utf-8'))
                            size -= len(chunk)
                            if(size <= 0):
                                break
                            chunk = conn.recv(1024)
                    conn.send(bytes("ok","utf-8"))
                    val = conn.recv(64)
                    if(str(val,"utf-8") == "execute"):
                        print("Executing..")
                        output = subprocess.check_output(["python3","job.py"])
                        # print("output:" + output)
                        with open("output.txt","w+") as o:
                            o.write(str(output,"utf-8"))
                        with open("output.txt","r") as a:
                            chunk = a.read(256)
                            print("Sending " + chunk)
                            while chunk:
                                print("Sending: " + chunk)
                                conn.send(bytes(chunk,"utf-8"))
                                chunk = a.read(256)
                        fs.flush()
                        print("Output sent.")
                        # print(output)
                        # os.system('py job.py')
                elif(str(task,"utf-8") == "PING"):
                    print("PING received.")
